package c.bidjoka.tpandroid.model;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.google.gson.annotations.Expose;

@Table(name = "mesures")
public class Measurement extends Model {

    /*
    @Column(name = "location", index = true, unique = true, onUniqueConflict = Column.ConflictAction.REPLACE)
    public String location;*/

    @Expose
    public String parameter;
    @Expose
    public Double value;
    @Expose
    private String lastUpdated;
    @Expose
    private String unit;

}
