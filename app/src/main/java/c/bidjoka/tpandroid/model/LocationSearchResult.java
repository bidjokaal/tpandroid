package c.bidjoka.tpandroid.model;

import com.google.gson.annotations.Expose;

import java.util.List;

public class LocationSearchResult {

    @Expose
    public List<Location> results;

}
