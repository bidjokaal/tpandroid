package c.bidjoka.tpandroid.model;

import com.google.gson.annotations.Expose;

import java.util.List;

public class ZoneSearchResult {

    @Expose
    public List<Zone> results;

}
