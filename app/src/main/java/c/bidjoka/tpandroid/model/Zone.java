package c.bidjoka.tpandroid.model;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;
import com.google.gson.annotations.Expose;

@Table(name = "Zone")
public class Zone extends Model {

    @Expose
    @Column(name = "city", index = true, unique = true, onUniqueConflict = Column.ConflictAction.REPLACE)
    public String city;

}
