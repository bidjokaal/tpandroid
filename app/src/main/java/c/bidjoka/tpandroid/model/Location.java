package c.bidjoka.tpandroid.model;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;
import com.google.gson.annotations.Expose;

import java.util.List;

@Table(name = "location")
public class Location extends Model {

    @Expose
    @Column(name = "location", index = true, unique = true, onUniqueConflict = Column.ConflictAction.REPLACE)
    public String location;
    @Expose
    @Column(name = "city")
    public String city;
    @Expose
    public List<Measurement> measurements = null;
    @Expose
    public Coordinates coordinates;
    @Column(name = "mesure")
    public String mesure;

    public Coordinates getCoordinates() {
        if (coordinates == null) {
            coordinates = new Select().from(Coordinates.class).where("location='" + location.replace("'", "''") + "'").executeSingle();
        }
        return coordinates;
    }
}
