package c.bidjoka.tpandroid.model;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.google.gson.annotations.Expose;

@Table(name = "coordinates")
public class Coordinates extends Model {

    @Column(name = "location", index = true, unique = true, onUniqueConflict = Column.ConflictAction.REPLACE)
    public String location;

    @Expose
    @Column(name = "longitude")
    public Double longitude;

    @Expose
    @Column(name = "latitude")
    public Double latitude;

}
