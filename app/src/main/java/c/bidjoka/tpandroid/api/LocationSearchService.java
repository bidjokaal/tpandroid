package c.bidjoka.tpandroid.api;

import android.util.Log;

import com.activeandroid.ActiveAndroid;
import com.activeandroid.query.Select;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.lang.reflect.Modifier;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import c.bidjoka.tpandroid.event.EventBusManager;
import c.bidjoka.tpandroid.event.LocationResultEvent;
import c.bidjoka.tpandroid.model.Location;
import c.bidjoka.tpandroid.model.LocationSearchResult;
import c.bidjoka.tpandroid.model.Measurement;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Query;

public class LocationSearchService {

    private static final long REFRESH_DELAY = 650;
    public static LocationSearchService INSTANCE = new LocationSearchService();
    private final LocationSearchRESTService mLocationSearchRESTService;
    private ScheduledExecutorService mScheduler = Executors.newScheduledThreadPool(1);
    private ScheduledFuture mLastScheduleTask;

    public LocationSearchService() {
        // Create GSON Converter that will be used to convert from JSON to Java
        Gson gsonConverter = new GsonBuilder()
                .excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT, Modifier.STATIC)
                .serializeNulls()
                .excludeFieldsWithoutExposeAnnotation().create();

        // Create Retrofit client
        Retrofit retrofit = new Retrofit.Builder()
                // Using OkHttp as HTTP Client
                .client(new OkHttpClient())
                // Having the following as server URL
                .baseUrl("https://api.openaq.org/v1/")
                // Using GSON to convert from Json to Java
                .addConverterFactory(GsonConverterFactory.create(gsonConverter))
                .build();

        // Use retrofit to generate our REST service code
        mLocationSearchRESTService = retrofit.create(LocationSearchRESTService.class);
    }

    //recherche des locations d'une zone en provenance de l'API
    public void searchLocationFromZone(final String search) {

        // Cancel last scheduled network call (if any)
        if (mLastScheduleTask != null && !mLastScheduleTask.isDone()) {
            mLastScheduleTask.cancel(true);
        }

        // Schedule a network call in REFRESH_DELAY ms
        mLastScheduleTask = mScheduler.schedule(new Runnable() {
            public void run() {
                // Step 1 : first run a local search from DB and post result
                searchLocationFromDB(search);
                mLocationSearchRESTService.searchForLocation(search).enqueue(new Callback<LocationSearchResult>() {
                    @Override
                    public void onResponse(Call<LocationSearchResult> call, Response<LocationSearchResult> response) {

                        // Post an event so that listening activities can update their UI
                        if (response.body() != null && response.body().results != null) {
                            // Save all results in Database
                            ActiveAndroid.beginTransaction();
                            for (Location local : response.body().results) {
                                local.coordinates.location = local.location;
                                local.mesure = concat(local.measurements );
                                local.save();
                                local.coordinates.save();
                            }
                            ActiveAndroid.setTransactionSuccessful();
                            ActiveAndroid.endTransaction();
                            // Send a new event with results from network
                            searchLocationFromDB(search);
                        } else {
                            // Null result
                            // We may want to display a warning to user (e.g. Toast)

                            Log.e("[AirQuality] [REST]", "Response error : null body");
                        }
                    }

                    @Override
                    public void onFailure(Call<LocationSearchResult> call, Throwable t) {
                        /// Request has failed or is not at expected format
                        // We may want to display a warning to user (e.g. Toast)
                        Log.e("[AirQuality] [REST]", "Response error : " + t.getMessage());

                    }

                });

            }
        }, REFRESH_DELAY, TimeUnit.MILLISECONDS);

    }

    //recherche des locations d'une zone en provenance de la DB
    public void searchLocationFromDB(String search) {
        // Get places matching the search from DB
        List<Location> matchingPlacesFromDB = new Select().
                from(Location.class)
                .where("city LIKE '%" + search + "%'")
                .orderBy("city")
                .execute();
        // Post result as an event
        EventBusManager.BUS.post(new LocationResultEvent(matchingPlacesFromDB));

    }

    public void searchLocation(String search){
        // Get places matching the search from DB
        List<Location> matchingPlacesFromDB = new Select().
                from(Location.class)
                .where("location LIKE '%" + search + "%'")
                .orderBy("location")
                .execute();
        // Post result as an event
        EventBusManager.BUS.post(new LocationResultEvent(matchingPlacesFromDB));
    }

    public String concat(List<Measurement> mesures){
        String tampon = "";
        for(int i=0; i < mesures.size(); i++){
            tampon = tampon + mesures.get(i).parameter+" "+ mesures.get(i).value+ " \n";
        }
        return tampon;
    }

    public interface LocationSearchRESTService {
        @GET("latest")
        Call<LocationSearchResult> searchForLocation(@Query("city") String search);
    }

}
