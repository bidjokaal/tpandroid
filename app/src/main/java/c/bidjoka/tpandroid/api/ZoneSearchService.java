package c.bidjoka.tpandroid.api;

import android.util.Log;

import com.activeandroid.ActiveAndroid;
import com.activeandroid.query.Select;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.lang.reflect.Modifier;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import c.bidjoka.tpandroid.event.EventBusManager;
import c.bidjoka.tpandroid.event.ZoneResultEvent;
import c.bidjoka.tpandroid.model.Zone;
import c.bidjoka.tpandroid.model.ZoneSearchResult;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Query;

public class ZoneSearchService {

    private static final long REFRESH_DELAY = 650;
    public static ZoneSearchService INSTANCE = new ZoneSearchService();
    private final ZoneSearchRESTService mZoneSearchRESTService;
    private ScheduledExecutorService mScheduler = Executors.newScheduledThreadPool(1);
    private ScheduledFuture mLastScheduleTask;



    public ZoneSearchService() {
        // Create GSON Converter that will be used to convert from JSON to Java
        Gson gsonConverter = new GsonBuilder()
                .excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT, Modifier.STATIC)
                .serializeNulls()
                .excludeFieldsWithoutExposeAnnotation().create();

        // Create Retrofit client
        Retrofit retrofit = new Retrofit.Builder()
                // Using OkHttp as HTTP Client
                .client(new OkHttpClient())
                // Having the following as server URL
                .baseUrl("https://api.openaq.org/v1/")
                // Using GSON to convert from Json to Java
                .addConverterFactory(GsonConverterFactory.create(gsonConverter))
                .build();

        // Use retrofit to generate our REST service code
        mZoneSearchRESTService = retrofit.create(ZoneSearchRESTService.class);
    }

    //recherche des zones de la France en provenance de l'API
    public void searchAllZone(){

        // Cancel last scheduled network call (if any)
        if (mLastScheduleTask != null && !mLastScheduleTask.isDone()) {
            mLastScheduleTask.cancel(true);
        }

        // Schedule a network call in REFRESH_DELAY ms
        mLastScheduleTask = mScheduler.schedule(new Runnable() {
            public void run() {
                // Step 1 : first run a local search from DB and post result
                searchAllZoneFromDB();
                // Call to the REST service
                mZoneSearchRESTService.searchForZones("FR").enqueue(new Callback<ZoneSearchResult>() {
                    @Override
                    public void onResponse(Call<ZoneSearchResult> call, retrofit2.Response<ZoneSearchResult> response) {
                        // Post an event so that listening activities can update their UI
                        if (response.body() != null && response.body().results != null) {
                            // Save all results in Database
                            ActiveAndroid.beginTransaction();
                            for (Zone zone : response.body().results) {
                                //récupération du nom de la zone pour mise en base de données
                                zone.save();
                            }
                            // Send a new event with results from network
                            ActiveAndroid.setTransactionSuccessful();
                            ActiveAndroid.endTransaction();

                            searchAllZoneFromDB();

                        } else {
                            // Null result
                            // We may want to display a warning to user (e.g. Toast)

                            Log.e("[Airquality] [REST]", "Response error : null body");
                        }
                    }

                    @Override
                    public void onFailure(Call<ZoneSearchResult> call, Throwable t) {
                        // Request has failed or is not at expected format
                        // We may want to display a warning to user (e.g. Toast)
                        Log.e("[Airquality] [REST]", "Response error : " + t.getMessage());
                    }
                });
            }
        },REFRESH_DELAY, TimeUnit.MILLISECONDS);
    }

    public void searchAllZoneFromDB() {
        List<Zone> matchingPlacesFromDB = new Select().
                from(Zone.class)
                .orderBy("city")
                .execute();
        // Post result as an event
        EventBusManager.BUS.post(new ZoneResultEvent(matchingPlacesFromDB));
    }

    public void searchZoneFromDB(String search){
        List<Zone> matchingPlacesFromDB = new Select().
                from(Zone.class)
                .where("city LIKE '%" + search + "%'")
                .orderBy("city")
                .execute();
        // Post result as an event
        EventBusManager.BUS.post(new ZoneResultEvent(matchingPlacesFromDB));
    }

    public interface ZoneSearchRESTService {

        @GET("cities")
        Call<ZoneSearchResult> searchForZones(@Query("country") String country);
    }

}
