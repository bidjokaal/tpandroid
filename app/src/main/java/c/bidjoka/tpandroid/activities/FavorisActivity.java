package c.bidjoka.tpandroid.activities;

import androidx.appcompat.app.AppCompatActivity;
import c.bidjoka.tpandroid.MainActivity;
import c.bidjoka.tpandroid.R;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

public class FavorisActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favoris);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.bottom_navigation_menu, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.accueil:
                this.accueil();
                return true;
            case R.id.zone:
                this.zone();
                return true;
            case R.id.carte:
                this.carte();
                return true;
            case R.id.favoris:
                this.favoris();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void accueil(){
        startActivity(new Intent(this, MainActivity.class));
    }

    private void zone(){
        startActivity(new Intent(this, ZoneActivity.class));
    }

    private void carte(){
        startActivity(new Intent(this, ZoneCarteActivity.class));
    }

    private void favoris(){
        startActivity(new Intent(this, FavorisActivity.class));
    }
}
