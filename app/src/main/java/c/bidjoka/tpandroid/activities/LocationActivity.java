package c.bidjoka.tpandroid.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import c.bidjoka.tpandroid.MainActivity;
import c.bidjoka.tpandroid.R;
import c.bidjoka.tpandroid.api.LocationSearchService;
import c.bidjoka.tpandroid.event.EventBusManager;
import c.bidjoka.tpandroid.event.LocationResultEvent;
import c.bidjoka.tpandroid.ui.LocationAdapter;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.squareup.otto.Subscribe;

import java.util.ArrayList;

public class LocationActivity extends AppCompatActivity {

    @BindView(R.id.location_recyclerView)
    RecyclerView mRecyclerView;
    private LocationAdapter mLocationAdapter;
    @BindView(R.id.location_loader)
    ProgressBar mProgressBar;
    @BindView(R.id.location_edittext)
    EditText mSearchEditText;

    private String mZoneValue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location);

        // Binding ButterKnife annotations now that content view has been set
        ButterKnife.bind(this);
        mZoneValue = getIntent().getStringExtra("zoneName");

        // Instanciation d'un LocationAdpater vide
        mLocationAdapter = new LocationAdapter(this, new ArrayList<>());
        mRecyclerView.setAdapter(mLocationAdapter);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        // Set textfield value according to intent
        if (getIntent().hasExtra("currentSearch")) {
            mSearchEditText.setText(getIntent().getStringExtra("currentSearch"));
        }

        mSearchEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                // Nothing to do when texte is about to change
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                // While text is changing, hide list and show loader
            }

            @Override
            public void afterTextChanged(Editable editable) {
                // Once text has changed
                // Show a loader
                mProgressBar.setVisibility(View.VISIBLE);

                // Launch a search through the PlaceSearchService
                LocationSearchService.INSTANCE.searchLocation(editable.toString());
            }
        });
    }

    @Override
    protected void onResume() {
        // Do NOT forget to call super.onResume()
        super.onResume();

        // Register to Event bus : now each time an event is posted, the activity will receive it if it is @Subscribed to this event
        EventBusManager.BUS.register(this);

        // Refresh search
        LocationSearchService.INSTANCE.searchLocationFromZone(mZoneValue);
    }

    @Override
    protected void onPause() {
        // Unregister from Event bus : if event are posted now, the activity will not receive it
        EventBusManager.BUS.unregister(this);

        super.onPause();
    }

    @Subscribe
    public void searchResult(final LocationResultEvent event) {
        // Here someone has posted a SearchResultEvent
        runOnUiThread (() -> {
            // Step 1: Update adapter's model
            mLocationAdapter.setLocations(event.getLocations());
            mLocationAdapter.notifyDataSetChanged();

            // Step 2: hide loader
            mProgressBar.setVisibility(View.GONE);
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.bottom_navigation_menu, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.accueil:
                this.accueil();
                return true;
            case R.id.zone:
                this.zone();
                return true;
            case R.id.carte:
                this.carte();
                return true;
            case R.id.favoris:
                this.favoris();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void accueil(){
        startActivity(new Intent(this, MainActivity.class));
    }

    private void zone(){
        startActivity(new Intent(this, ZoneActivity.class));
    }

    private void carte(){
        startActivity(new Intent(this, ZoneCarteActivity.class));
    }

    private void favoris(){
        startActivity(new Intent(this, FavorisActivity.class));
    }
}
