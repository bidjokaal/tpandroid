package c.bidjoka.tpandroid.ui;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.IOException;
import java.util.List;

import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import c.bidjoka.tpandroid.R;
import c.bidjoka.tpandroid.activities.LocationDetailActivity;
import c.bidjoka.tpandroid.model.Location;

public class LocationAdapter extends RecyclerView.Adapter<LocationAdapter.LocationViewHolder>{

    private LayoutInflater inflater;
    private Activity context;
    private List<Location> mLocations;

    public LocationAdapter(Activity context, List<Location> locations) {
        inflater = LayoutInflater.from(context);
        this.context = context;
        this.mLocations = locations;
    }

    @Override
    public LocationAdapter.LocationViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.location_item, parent, false);
        LocationAdapter.LocationViewHolder holder = new LocationAdapter.LocationViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(LocationAdapter.LocationViewHolder holder, int position) {
        // Adapt the ViewHolder state to the new element
        final Location location = mLocations.get(position);
        holder.mLocationNameTextView.setText(location.location);
        holder.mLocationIndicatorTextView.setText(""+location.mesure);
        holder.mLocationIcon.setImageResource(R.drawable.marker);

        holder.mLocationIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Open place details activity
                Intent seeLocationIntent = new Intent(context, LocationDetailActivity.class);
                seeLocationIntent.putExtra("locationName", location.location);
                context.startActivity(seeLocationIntent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return mLocations.size();
    }

    public void setLocations(List<Location> locations) {
        this.mLocations = locations;
    }

    // Pattern ViewHolder
    class LocationViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.location_name)
        TextView mLocationNameTextView;

        @BindView(R.id.location_indicateur)
        TextView mLocationIndicatorTextView;

        @BindView(R.id.location_adapter_icon)
        ImageView mLocationIcon;

        public LocationViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
