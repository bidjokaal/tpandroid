package c.bidjoka.tpandroid.ui;


import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import c.bidjoka.tpandroid.R;
import c.bidjoka.tpandroid.activities.LocationActivity;
import c.bidjoka.tpandroid.model.Zone;

public class ZoneAdapter extends RecyclerView.Adapter<ZoneAdapter.ZoneViewHolder>{

    private LayoutInflater inflater;
    private Activity context;
    private List<Zone> mZones;

    public ZoneAdapter(Activity context, List<Zone> zones) {
        inflater = LayoutInflater.from(context);
        this.context = context;
        this.mZones = zones;
    }

    @Override
    public ZoneViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.zone_item, parent, false);
        ZoneAdapter.ZoneViewHolder holder = new ZoneAdapter.ZoneViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(ZoneViewHolder holder, int position) {
        final Zone zone = mZones.get(position);
        holder.mZoneNameTextView.setText(zone.city);
        holder.mZoneIcon.setImageResource(R.drawable.zone);

        holder.mZoneIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Open place details activity
                Intent seeLocationIntent = new Intent(context, LocationActivity.class);
                seeLocationIntent.putExtra("zoneName", zone.city);
                context.startActivity(seeLocationIntent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return mZones.size();
    }

    public void setmZones(List<Zone> zones){
        this.mZones = zones;
    }

    class ZoneViewHolder extends RecyclerView.ViewHolder{
        @BindView(R.id.zone_name)
        TextView mZoneNameTextView;

        @BindView(R.id.zone_adapter_icon)
        ImageView mZoneIcon;

        public ZoneViewHolder(View itemView){
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}


