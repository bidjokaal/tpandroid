package c.bidjoka.tpandroid.event;

import java.util.List;

import c.bidjoka.tpandroid.model.Zone;

public class ZoneResultEvent {

    private List<Zone> zones;

    public ZoneResultEvent(List<Zone> zones){

        this.zones = zones;
    }
    public List<Zone> getZones(){
        return zones;
    }
}
