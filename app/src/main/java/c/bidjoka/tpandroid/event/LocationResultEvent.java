package c.bidjoka.tpandroid.event;

import java.util.List;

import c.bidjoka.tpandroid.model.Location;

public class LocationResultEvent {

    private List<Location> locations;

    public LocationResultEvent(List<Location>locations){
        this.locations = locations;
    }

    public List<Location> getLocations(){
        return locations;
    }

}
